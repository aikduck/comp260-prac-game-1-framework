﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

    public Vector2 velocity; // in metres per second
    public float maxSpeed = 5.0f;
    public string horizontalAxisName = "Horizontal";
    public string verticalAxisName = "Vertical";

    void Update()
    {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis(horizontalAxisName);
        direction.y = Input.GetAxis(verticalAxisName);

        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;

        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }


}
